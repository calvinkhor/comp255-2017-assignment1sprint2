
package org.bitbucket.sudoku;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.bitbucket.sudoku.Sudoku;

/**
 * Tests that the set of values for each box is correct
 */
@RunWith( Parameterized.class )
public class BoxIndicesTests {

  /** Grid of size 4 */
  static int[][] g4 = new int[][] { {0, 1, 2, 3},
                                    {4, 5, 6, 7},
                                    {8, 9, 10,11},
                                    {12, 13, 14, 15}};

  /** Grid of size 9 */
  static int[][] g9 = new int[][] { {0, 1, 2, 3, 4, 5, 6, 7, 8},
                                    {9, 10, 11, 12, 13, 14, 15, 16, 17},
                                    {18, 19, 20, 21, 22, 23, 24, 25, 26},
                                    {27, 28, 29, 30, 31, 32, 33, 34, 35},
                                    {36, 37, 38, 39, 40, 41, 42, 43, 44},
                                    {45, 46, 47, 48, 49, 50, 51, 52, 53},
                                    {54, 55, 56, 57, 58, 59, 60, 61, 62},
                                    {63, 64, 65, 66, 67, 68, 69, 70, 71},
                                    {72, 73, 74, 75, 76, 77, 78, 79, 80}};
  /**
   *  The collection of tests as an Array of (int x expected result)
   */
  /* *INDENT-OFF* */
   @Parameters(name = "{index}: set of values for box {1} should be {2}")
   public static Collection<Object[]> data() {
     // testcases wil de displayed as test[0], test[1] and so on
     return Arrays.asList(new Object[][] {
       //   grid of size 1
       { new int[][]
           { { 0 } }, 0,
           (Stream.of(0)).collect(Collectors.toSet())
       },
       //   Grid of size 4 x 4
       { g4 , 0 , (Stream.of(0, 1, 4, 5)).collect(Collectors.toSet())},
       { g4 , 1 , (Stream.of(2, 3, 6, 7)).collect(Collectors.toSet())},
       { g4 , 2 , (Stream.of(8, 9, 12, 13)).collect(Collectors.toSet())},
       { g4 , 3 , (Stream.of(10, 11, 14, 15)).collect(Collectors.toSet())},
       //   Grid of size 9 x 9
       { g9 , 0 , (Stream.of(0, 1, 2, 9, 10, 11, 18, 19, 20)).collect(Collectors.toSet())},
       { g9 , 1 , (Stream.of(3, 4, 5, 12, 13, 14, 21, 22, 23)).collect(Collectors.toSet())},
       { g9 , 2 , (Stream.of(6, 7, 8, 15, 16, 17, 24, 25, 26)).collect(Collectors.toSet())},
       { g9 , 3 , (Stream.of(27, 28, 29, 36, 37, 38, 45, 46, 47)).collect(Collectors.toSet())},
       { g9 , 4 , (Stream.of(30, 31, 32, 39, 40, 41, 48, 49, 50)).collect(Collectors.toSet())},
       { g9 , 5 , (Stream.of(33, 34, 35, 42, 43, 44, 51, 52, 53)).collect(Collectors.toSet())},
       { g9 , 6 , (Stream.of(54, 55, 56, 63, 64, 65, 72, 73, 74)).collect(Collectors.toSet())},
       { g9 , 7 , (Stream.of(57, 58, 59, 66, 67, 68, 75, 76, 77 )).collect(Collectors.toSet())},
       { g9 , 8 , (Stream.of(60, 61, 62, 69, 70, 71, 78, 79, 80)).collect(Collectors.toSet())},
     });
   }
  /* *INDENT-ON* */

  // Test parameters
  // @link{https://github.com/junit-team/junit4/wiki/Parameterized-tests}
  private int[][] g;
  private int boxNumber;
  private Set<Integer> setOfIndices;

  /**
   *  Constructor for the tests
   */
  public BoxIndicesTests(int[][] input, int box, Set<Integer> expectedSet) {
    g = input;
    boxNumber = box;
    setOfIndices = expectedSet;
  }

  /**
   * Run all the tests
   */
  @Test
  public void test()  {
    assertEquals(setOfIndices, new Sudoku (g).getValuesInBox( boxNumber, g.length));
  }
}
