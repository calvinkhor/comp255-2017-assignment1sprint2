
package org.bitbucket.sudoku;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * A Sudoku
 */
public class Sudoku {

  /**
   * Constructor for Sudoku
   *
   * @param g   The grid that defines the sudoku
   */
  public Sudoku(int[][] g) {
    theGrid = g;
  }

  /**
   * Secondary constructor for Sudoku
   *
   * @param g   The grid that defines the sudoku
   * @param e   The value that denotes an empty cell
   */
  public Sudoku(int[][] g, int e) {
    // FIXME
  }
  /**
   * The n x m grid that defines the Sudoku
   */
  private int[][] theGrid;

  /**
   *  Check whether an integer is a square
   *
   *  @return true if and only if k = n * n for some n
   */
  static boolean isSquare(int k) {
    // Compute square root of k as a double
    double sqrtOf = Math.sqrt(k);
    return ((int) sqrtOf ) * ((int) sqrtOf ) == k;
  }

  /**
   *   Collect values in a line of the Sudoku
   *
   *   @param   k   The index of the line to collect the values of
   *   @return      The set of integers on the line k
   */
  private Set<Integer> getValuesInLine(int k) {
    return IntStream.range(0, theGrid.length).
           map(i -> theGrid[ k ][ i ]).
           boxed().
           collect(Collectors.toSet());
  }

  /**
   *   Collect values in a column of the Sudoku
   *
   *   @param   k   The index of the column to collect the values of
   *   @return      The set of integers on the column k
   */
  private Set<Integer> getValuesInColumn(int k) {
    return IntStream.range(0, theGrid.length).
           map(i -> theGrid[ i ][ k ]).
           boxed().
           collect(Collectors.toSet());
  }

  /**
   *   Collect values in a box of the Sudoku
   *
   *   @param   k   The index of the box to collect the values of
   *   @param   n   The size of the grid (should be a square)
   *   @return      The set of integers in the box k
   */
  Set<Integer> getValuesInBox(int k, int n) {

    // Assuming n is a square of the form j * j with j an int, j is obtained by:
    int j = (int) Math.sqrt(n);

    // The boxes are numbered from 0 to theGrid.length - 1, starting from
    // top left and enumerated from left to right, top to bottom.
    // The coordinates of the `theGrid.length` cells in box k are in the set
    // ii / j + ( j * ( k / j )), i % j + j * ( k % j ),
    // for i ranging over 0..theGrid.length - 1
    return IntStream.range(0, theGrid.length).
           //  Map interavl to the set of cells in box k
           map(i -> theGrid[ i / j + ( j * ( k / j )) ][ i % j + j * ( k % j ) ]).
           //  Make an Integer for each of them
           boxed().
           //  Flatmap and get the set of values in the box
           collect(Collectors.toSet());
  }

  /**
   *  Check validity of a Sudoku grid
   *
   *  @return true if and only if theGrid is a valid Sudoku
   */
  public boolean isValid()  {
    if (theGrid.length == 0)
      // Grid is empty
      return true;

    else if (!isSquare(theGrid.length))
      // Grid size is not square
      return false;

    else if (IntStream.range(0, theGrid.length).
             anyMatch( i -> ( theGrid[ i ].length != theGrid.length ) ) )
      // At least one row has a size different to the number of rows
      return false;

    else {
      //  Height and width match and grid is not empty, of square size
      //  Compute the set of numbers that should be in the Sudoku
      //  Should be 1 to theGrid.length
      Set<Integer> refSet = IntStream.range(1, theGrid.length + 1).
                            boxed().
                            collect(Collectors.toSet());

      // Check all the lines against this reference set
      if (IntStream.range(0, theGrid.length).
          anyMatch( i ->  !getValuesInLine(i).equals( refSet ) ) )
        // At least one row has a set of values different to the number of rows
        return false;

      //  Check the columns
      else if (IntStream.range(0, theGrid.length).
               anyMatch( i -> !getValuesInColumn(i).equals( refSet ) ) )
        //  At least one column has a set of values different to refSet
        return false;

      // Check all the boxes
      else if (IntStream.range(0, theGrid.length).
               anyMatch( i -> !getValuesInBox(i, theGrid.length).equals( refSet ) ) )
        //  At least one box has a set of values different to refSet
        return false;

      //  Lines, columns and boxes satisfy OneRule
      else
        return true;
    }
  }

  /**
   *   Attempt to compute a solution to the Sudoku
   *
   *   @return  A grid with possibly less empty cells than in theGrid (but not more)
   *
   *   @note    If there is no empty cell in the result, then the Sudoku is solved,
   *            otherwise it is not
   */
  public int[][] solve() {
    // FIXME: for now this is OK
    return theGrid;
  }
}
